(function() {
	var chat = {
		partners: {},
		user: {},
		userInfo: function(user) {
			$.each(user, function(index, value) {
				chat.user[index] = value;
			});
		},
		initiateChat: function(partner) {
			if(!chat.partner['partner' + partner]) {
				$.get({
					url: 'userinfo.php',
					success: function(data) {
						chat.partner['partner' + partner] = data;
						var chatbox = ('#chatbox');
						var div = $(document.createElement('div'));
						var title = $(document.createElement('span'));
						title.html('Chat with ' + data.firstname + ' ' + data.lastname);
						title.attr({
							id: 'title' + partner,
						});
						var chatbody = $(document.createElement('div'));
						chatbody.attr({
							id: 'chatbody' + partner,
							className: 'chatbody'
						});
						var submitButton = $(document.createElement('input'));
						submitButton.attr({
							type: 'submit'
						});
						submitButton.val('Send');
						var textbox = $(document.createElement('input'));
						textbox.attr({
							type: 'text',
							size: '35',
							name: 'message',
							id: 'textbox' + partner,
						});
						textbox.on('focus', function(event) {
							$('#title' + partner).css({
								backgroundColor: 'lightgray'
							});
						});
						var form = $(document.createElement('form'));
						form.attr({
							partner: partner,
							id: 'form' + partner,
							className: 'chatForm'
						});
						form.append(textbox, submitButton);
						div.append(title, $(document.createElement('ht')), chatbody, form);
						chatbox.append(div);
					}
				});
			}
		}
	};

	$('#changePass').submit(function(event) {
		if($('#password').val() != $('#confpass').val()) {
			alert('Passwords no not match.!');
			return false;
		}
	});

	chat.onlineUsers = setInterval(function() {
		$.get({
			url: 'onlineusers.php',
			success: function(data) {
				var ou = $('#onlineUser');
				var ul = $(document.createElement('ul'));
				$.each(data, function(index, value) {
					var partner = value.id;
					var status = $(document.createElement('img'));
					status.attr({
						src: value.online ? 'online.png' : 'offline.png'
					});
					var name = $(document.createElement('span'));
					name.html(chat.partner['partner' + partner].firstname + ' ' + chat.partner['partner' + partner].lastname);
					var li = $(document.createElement('li'));
					li.append(status, name);
					li.click(function(event) {
						chat.initiateChat(partner);
					});
					ul.append(li);
				});
				ou.html(ul);
			}
		});
	}, 2500);

	chat.conversation = setInterval(function() {
		$.get({
			url: 'receive.php',
			success: function(data) {
				$.each(data, function(index, value) {
					if(!chat.partner['partner' + value.id]) {
						chat.initiateChat(value.id);
					}
					var chatbody = $('#chatbody' + value.id);
					var name = $(document.createElement('span'));
					name.html(chat.partner['partner' + value.id].firstname + ': ');
					name.css({
						fontWeight: 'bold'
					});
					var message = $(document.createElement('span'));
					message.html(value.message);
					chatbody.append(name, message);
				});
			}
		});
	}, 1000);

	$('.chatForm').submit(function(event) {
		var partner = $(event.target).attr('partner');
		var text = $('#textbox' + partner);
		$.post({
			url: 'send.php',
			data: {
				sender: chat.user.id,
				recepient: partner,
				message: text.val()
			}
		});
		text.val('');
		return false;
	});

	window.chat = chat;
})();